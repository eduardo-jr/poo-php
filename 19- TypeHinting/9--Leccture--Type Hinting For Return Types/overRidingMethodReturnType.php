<?php 
class A{
	public function check():int{
		echo "hello";
	}
}
class B extends A{
	public function check():int{
		echo "hello";
	}
}


/*

	class A{
		public function check():string{
			return "hello";
		}
	}
	class B extends A{
		public function check():string{
			return "hello";
		}
	}

	$n = new B();
	echo $n->check();

 */