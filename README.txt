Curso basado en PHP 7 del Mayo 2017

Overview of the course
1 Sections Overview

Introduction to Object Oriented Programming
2 Introduction To Object Oriented Programming

Basics of Object Oriented Programming
3 Classes And Objects
4 Properties And Methods
5 Creating Classes And Objects In PHP
6 Creating And Accessing Properties
7 Print The Whole Object
8 Defining And Calling Methods
9 Accessing Object Properties From Methods
10 Parameters And Return Value Of Methods
11 PHP Source Files Used In _Basics of Object Oriented Programming_ Lectures
12 Solution_ Simple Calculator

Visibility
13 Visibility
14 Php Source Files Used In _Visibility_ Section

Class Constants
15 Class Constants
16 Php Source Files Used In _Class Constants_ Section

Data Encapsulation
17 Data Encapsulation
18 Php Source Files Used In _Data Encapsulation_ Section

Inheritance
19 Inheritance
20 Real Life Example Of Inheritance
21 Visibility Level Protected In Inheritance
22 ISA Vs HASA
23 Php Source Files Used In _Inheritance_ Section

Overriding
24 Overriding
25 Preserving Parent Class Functionality in overriding
26 Php Source Files Used In _Overriding_ Section

Final Keyword
27 Final Classes And Methods
28 Php Source Files Used In _Final Keyword_ Section

Abstract classes and methods
29 Abstract Classes And Methods
30 Php Source Files Used In _Abstract Classes And Methods_ Section

Interfaces
31 Interfaces
32 The difference between abstract classes and interfaces
33 Php Source Files Used In _Interfaces_ Section

Constructor and Destructor
34 What Is Constructor And Destructor_
35 Constructor Using __construct() Magic Method
36 Destructor Using __destruct() Magic Method
37 Php Source Files Used In _Constructor And Destructor_ Section

Static Properties And Methods
38 Static Keyword And Static Properties
39 Static Methods
40 Php Source Files Used In _Static Properties And Methods_ Section

Magic Methods
41 Magic Methods
42 Magic Methods In Different Topics
43 __toString() Magic Method
44 __set_state() Magic Method
45 __invoke() Magic Method
46 __debugInfo() Magic Method
47 Php Source Files Used In _Magic Methods_ Section

Errors and exceptions in php
48 Introduction to Errors And Exception
49 Errors and Their Types
50 Sub Types Of Errors
51 Notices
52 Warnings
53 Fatal Errors
54 Parser Errors
55 Strict Standards Notices
56 E_ALL And Error Level Constants
57 Triggering Errors
58 When These Errors Occur In Life Cycle Of Your Script
59 Logical Errors
60 Error Reporting Settings In php ini
61 What To Do With Errors_
62 Display Errors
63 Logging Your Own Error Messages
64 How To Handle Errors With Error Handler
65 What Actually The Exceptions Are What They Actually Do_
66 Try_ Throw and Catch
67 The Exception Class In PHP
68 Stack Trace For Exception
69 Creating Your Own Custom Exceptions
70 Try With Multiple Catch Blocks
71 Re-Throwing Exceptions
72 Uncaught Exceptions
73 Top Level Exception Handler
74 The Change In Errors and Exceptions In PHP 7
75 Php Source Files Used In _Errors And Exceptions In PHP_ Section

Autoloading
76 Autoloading
77 Autoloader Function
78 How To Register An Autoloader Function
79 Namespaces
80 Autoloading Namespaces
81 Psr-0 And Psr-4 Autoloading Standards
82 Autoloader To Autoload Namespace Classes
83 Php Source Files Used In _Autoloading_ Section

Object Serialization
84 Object Serialization
85 serialize() and unserialize()
86 Magic Methods For Serialization __sleep() and __wakeup()
87 Php Source Files Used In _Object Serialization_ Section

Object Cloning
88 Object Cloning
89 Shallow Copy Cloning
90 Deep Copy Cloning Using __clone() Magic Method
91 Recursive Cloning
92 Double Linking Problem
93 Solution Deep Copy Cloning Using serialization
94 Php Source Files Used In _Object Cloning_ Section

Type Hinting
95 Type Hinting
96 Type Hinting For Non Scalar Data Types
97 Type Hinting For Class And Objects
98 Type Hinting For Interfaces
99 Type Hinting For Self
100 Type Hinting For Arrays
101 Type Hinting For Callable
102 Type Hinting For Scalar Data Types
103 Strict Scalar Type Hinting
104 Type Hinting For Return Types
105 Type Error Exception In PHP7
106 Php Source Files Used In _Type Hinting_ Section

Comparing Objects
107 Object Comparison Using (==) And (===)
108 Php Source Files Used In _Comparing Objects_ Section

Overloading
109 Overloading
110 Property Overloading Using Magic Methods (__get___set_ __isset and __unset)
111 Method Overloading Using Magic Methods (__call_ __callStatic)
112 Php Source Files Used In _Overloading_ Section

Traits
113 Multiple Inheritance And Deadly Diamond Of Death
114 Single Inheritance And Its Limitation
115 Php Traits
116 Precedence
117 Multiple Traits
118 Traits Composed From Traits
119 Abstract And Static Members Of Traits
120 Trait Properties
121 Php Source Files Used In _Traits_ Section

Late Static Binding
122 Binding_ Early Binding And Late Binding
123 Problem_ _Early Binding of self Keyword_
124 Solution_ _Late Static Binding Using Static Keyword
125 Php Source Files Used In _Late Static Binding_ Section

Object Iteration
126 Traverse_ Iterate_ Iterate Using Loops
127 Object Iteration
128 Iterator Interface
129 IteratorAggregate Interface
130 Iterator And IteratorAggregate Interface In One Plate
131 Php Source Files Used In _Object Iteration_ Section