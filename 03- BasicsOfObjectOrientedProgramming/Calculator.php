<?PHP
class Calculator
{

	public function add( $number1, $number2 ) {
		if( is_numeric( $number1 ) && is_numeric( $number2 ) ) {
			return $number1+$number2;
		} else {
			echo "values should be numeric.";
		}
	}

	public function subtract( $number1, $number2 ){
		if( is_numeric( $number1 ) && is_numeric( $number2 ) ) {
			return $number1-$number2;
		} else {
			echo "values should be numeric.";
		}
	}

	public function multiply( $number1, $number2 ){
		if( is_numeric( $number1 ) && is_numeric( $number2 ) ) {
			return $number1*$number2;
		} else {
			echo "values should be numeric.";
		}
	}

	public function divide( $number1, $number2 ) {
		if( is_numeric( $number1 ) && is_numeric( $number2 ) ) {
			return $number1/$number2;
		} else {
			echo "values should be numeric.";
		}
	}
}

$calc = new Calculator;

echo $calc->add( 1, 3 );
echo '<br><br>';
echo $calc->subtract( 4, 3 );
echo '<br><br>';
echo $calc->multiply( 2, 3 );
echo '<br><br>';
echo $calc->divide( 10, 2 );